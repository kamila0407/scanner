#Scanner - лексический анализатор интерпретатора подмножества языка паскаль,
#   включающего в себя циклы while, if (+else), арифметические операции, операцию присваивания,
#   а также некоторые встроенные функции.
#Сканер написан с использованием метода конечного автомата.
#На вход сканер принимает файл на pascal и файл-таблицу ключевых слов. Сканер обрабатывает входные данные,
#   создает и заполняет файл-таблицу имен и выдает файл - output,в котором сформирован поток лексем.
#Scanner обрабатывает ошибки такого типа:
#    1) "неправильные имена",такие, как 34ab
#    2) ошибки записи числа double, например, 12. или 12.xy
#    3) незакрытые кавычки
#    4) незакрытые скобки всех видов (,[,{
#    5) неправильный баланс скобок всех видов, например, }.....{ или (....))
#При нахождении ошибки программа указывает тип ошибки и завершается.


#Замилова Камила, МФТИ ИНБИКСТ, группа 4103
#16.12.2017

DIG = '0123456789'
LETTER = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM'
SYMBOL = r"+-*/,;()<>\[]!#"

#Функция Define(c) определяет "тип" символа
def Define(c):
    return {
        DIG.find(c) != -1 : 'int',
        LETTER.find(c) != -1 : 'str',
        SYMBOL.find(c) != -1 : 'sym',
        c == '.': 'point',
        0<= ord(c) <= 32 : 'esc',
        c == ':' : 'colon',
        c == '{' : 'com1',
        c == '}' : 'com2',
        c == '"' : 'quote',
        c == '=' : 'eq'
        }[1]

#Функция Check_Key() проверяет :
#        1) находится ли лексема в таблице ключевых слов
#        2) если да, записывает в файл-output сопоставляемое ей число
#        3) если нет, вызывает функцию Write_Name()
def Check_Key():
    global lex
    global i
    global q_num
    global new_key
    key_word = 0
    for j in range (0,len(new_key),2):
        if( lex == new_key[j] ):
            key_word = 1
            output = open('output.txt', 'a')
            output.write(str(new_key[j+1]) + ' ')
            if( lex == '"' and q_num%2==0):
                i = i + 1
            lex = ''
            break
    if(key_word == 0):
        Write_Name()
        
#Функция Write_Name() :
#        1) находится ли лексема в таблице имен
#        2) если да, записывает в файл-output сопоставляемое ей число
#        3) если нет, записывает лексему в таблицу имен и записывает в файл-output сопоставляемое ей число
def Write_Name ():
    global lex
    global i
    global q
    name = open('name_table.txt', 'a')
    name1 = open('name_table.txt').read()
    new_name = name1.split('\n')
    name_word = 0
    output = open('output.txt', 'a')
    if(q == 'C2' or q =='C3'):
        lex = lex[0:-2]
    for j in range (0,len(new_name),1):
        if( lex == new_name[j].split('%@&')[0]):
            output.write(str(new_name[j].split('%@&')[1]) + ' ')
            name_word = 1
            break
    
    if(name_word == 0):
        name.write(lex + '%@&')
        name.write(str(i) + '%@&')
        if( q == 'C1' or q == 'C3'):
            lex_type = 'var'
            lex_value = '0'
        else:
            lex_type = 'const'
            lex_value = lex
        name.write(lex_type + '%@&')
        if(q == 'D1' or q == 'C1' or q == 'C3'):
            lex_kind = 'int'
        elif(q == 'D2'):
            lex_kind = 'float'
        elif(q == 'C2'):
            lex_kind = 'label'
        else:
            lex_kind = 'str'
        
        name.write(lex_kind + '%@&')
        name.write(lex_value + '\n')
        output.write(str(i) + ' ')
    lex = ''

#Функция Add(c) :
#    1)добавляет к лексеме символ c
#    2)изменяет глобальные константы-счетчики кавычек и скобок 
def Add(c):
    global lex
    global q_num
    global b1_num
    global b2_num
    global m1_num
    global m2_num
    lex = lex + c
    if(lex == '"'):
        q_num = q_num + 1
    if(lex == ')'):
        b1_num = b1_num + 1
        if(b2_num<b1_num):
            print('Brackets () error!')
            exit(1)
    if(lex == '('):
        b2_num = b2_num + 1
    if(lex == ']'):
        m1_num = m1_num + 1
        if(m2_num<m1_num):
            print('Brackets [] error!')
            exit(1)
    if(lex == '['):
        m2_num = m2_num + 1

#Функция Ungetch() сдвигает курсор на один влево
def Ungetch():
    global i
    i = i - 1
    
#Функция Name_Error() информирует о неправильном имени переменной и завершает программу
def Name_Error():
    print('BAD NAME ERROR!')
    exit(1)

#Функция Float_Error() информирует о неправильном float-числе и завершает программу
def Float_Error():
    print('FLOAT ERROR!')
    exit(1)
    
#Функция Procedures ( F, c, i ) вызывает нужные функции в зависимости от примененного правила конечного автомата
def Procedures ( F, c, i ):
    global lex
    global q_num
    global q
    global c_count
    for f in F:
        if( f == 'Add'):
            Add(c)
        elif( f == 'Ungetch'):
            Ungetch()
        elif( f == 'Write_Name'):
            Write_Name()
        elif( f == 'Check_Key'):
            Check_Key()
        elif( f == 'Null_Lex'):
            lex = ''
        elif( f == 'Bad_Name_Error'):
            Name_Error()
        elif( f == 'Float_Error'):
            Float_Error()
        elif( f == 'Count_Comm'):
            c_count = c_count + 1
        else:
            print('DO NOT KNOW WHAT TO DO')
            exit(1)
            return 0
        
#Функция Scanner() - основная функция,содержащая правила конечного автомата.
#        1)считывает pascal-файл в строку
#        2)обрабатывает полученную посимвольно в зависимости от правила
def Scanner ():
    print('Enter the pascal-file\'s name :')
    char = open(input()).read()
    RULES = [ ['S', 'S', ['esc'],['Null_Lex'] ],
              ['S', 'C1', ['str'], ['Add']],
              ['C1', 'C1', ['str', 'int'], ['Add']],
              ['C1', 'S', ['int', 'esc', 'sym', 'point', 'eq', 'quote', 'com1', 'com2'], ['Check_Key', 'Ungetch']],
              ['C1', 'C2',['colon'],['Add']],
              ['C2','S', ['int', 'esc', 'sym', 'point', 'str', 'quote', 'com1', 'com2','colon'],['Add','Write_Name','Ungetch']],
              ['C2', 'C3', ['eq'], ['Add']],
              ['C3', 'S', ['int', 'esc', 'sym', 'point', 'str', 'quote', 'com1', 'com2','colon', 'eq'], ['Write_Name','Ungetch','Ungetch', 'Ungetch']],
              ['S', 'D1', ['int'], ['Add']],
              ['D1', 'D1', ['int'], ['Add']],            
              ['D1', 'D2', ['point'], ['Add']],
              ['D1', 'E1', ['str'], ['Bad_Name_Error']],
              ['D2', 'D3', ['int'], ['Add']],
              ['D3', 'D3', ['int'], ['Add']],
              ['D3', 'S', ['str', 'sym', 'point', 'eq', 'colon', 'esc', 'quote', 'com1', 'com2'], ['Write_Name', 'Ungetch']],
              ['D2', 'E2', ['str', 'sym', 'point', 'eq', 'colon', 'esc', 'quote', 'com1', 'com2'], ['Float_Error']],
              ['D1', 'S', ['str', 'sym', 'eq', 'colon', 'esc', 'quote', 'com1', 'com2'], ['Write_Name', 'Ungetch']],
              ['S', 'S', ['sym','point','eq'], ['Add', 'Check_Key']],
              ['S', 'M1', ['com1'],['Count_Comm'] ], 
              ['M1', 'M1', ['int', 'sym', 'point', 'eq', 'colon', 'esc', 'quote', 'com1', 'str'],[] ],
              ['M1', 'S', ['com2'],['Count_Comm'] ],
              ['S', 'Q1', ['quote'], ['Add', 'Check_Key']],
              ['Q1', 'Q1', ['int', 'sym', 'point', 'eq', 'colon', 'esc', 'str', 'com1', 'com2'], ['Add']],
              ['Q1', 'S', ['quote'], ['Write_Name', 'Ungetch', 'Add', 'Check_Key']],
              ['S', 'P1', ['colon'],['Add']],
              ['P1', 'S', ['eq'], ['Add', 'Check_Key']],
              ['P1', 'S', ['int', 'sym', 'point', 'str', 'colon', 'esc', 'quote', 'com1', 'com2'], ['Check_Key', 'Ungetch']],
            ]
    global lex             
    global i
    global q
    global q_num
    global b1_num
    global b2_num
    global m1_num
    global m2_num
    global c_count
    lex = ''
    q = 'S'
    i = 0
    while(i < len(char)):
        found = 0
        for L in RULES:
            if(L[0] == q):
                for t in L[2]: 
                    if( Define(char[i])== t):
                        found = 1
                        Procedures(L[3],char[i],i)
                        q = L[1]
                        i = i+1
                    
                    if(i == len(char)):
                        if(q_num%2 != 0):
                            print('Qotes error!')
                            exit(1)
                        if(b1_num != b2_num):
                            print('Brackets () error!')
                            exit(1)
                        if(m1_num != m2_num):
                            print('Brackets [] error!')
                            exit(1)
                        if(c_count%2!=0):
                            print('Unclosed comments!')
                            exit(1)
                        
                        if(q == 'C1' or q == 'D1' or q == 'D2' or q == 'P1'):
                            Check_Key()
                        
                    if(found == 1):
                        break
            if(found == 1):
                break

#Функция main() считывает таблицу ключевых слов в список и вызывает Scanner()
def main():
    global lex
    global i
    global q
    global q_num
    global b1_num
    global b2_num
    global m1_num
    global m2_num
    global c_count
    
    c_count = 0
    q_num = 0
    b1_num = 0
    b2_num = 0
    m1_num = 0
    m2_num = 0

    key = open('key_table.txt').read()
    global new_key
    new_key = key.split()
    Scanner()
    
if __name__ == '__main__':
    main()
